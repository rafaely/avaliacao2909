#!/bin/bash

echo -e "\033[01;36m ***Amar***\033[01;36m"

sleep 0.5

echo -e "\033[01;35m Que pode uma criatura senão,\033[01;35m"

sleep 0.5

echo -e "\033[01;32m entre criaturas, amar?,\033[01;32m"

sleep 0.5

echo -e "\033[01;33m amar e esquecer,\033[01;33m"

sleep 0.5

echo -e "\033[01;34m amar e malamar,\033[01;34m"

sleep 0.5

echo -e "\033[01;35m amar, desamar, amar?,\033[01;35m"

sleep 0.5

echo -e "\033[01;32m sempre, e ate de olhos vidrados, amar?,\033[01;32m"

sleep 0.5
